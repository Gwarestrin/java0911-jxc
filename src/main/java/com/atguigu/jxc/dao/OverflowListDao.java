package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author : Ziming
 * date : 2023/9/12
 * description:
 */
public interface OverflowListDao {
    Integer save(@Param("overflowList") OverflowList overflowList);

    List<OverflowList> list(@Param("sTime") String sTime, @Param("eTime") String eTime);
}
