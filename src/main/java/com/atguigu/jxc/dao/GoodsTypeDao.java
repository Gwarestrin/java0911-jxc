package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);

    String getTypeNameByTypeId(Integer goodsTypeId);

    Integer addGoodsType(@Param("goodsTypeName") String goodsTypeName, @Param("pId") Integer pId);

    /**
     * 根据id查GoodsType
     *
     * @param goodsTypeId
     * @return
     */
    GoodsType getGoodsTypeById(Integer goodsTypeId);

    Integer deleteById(@Param("goodsTypeId") Integer goodsTypeId);
}
