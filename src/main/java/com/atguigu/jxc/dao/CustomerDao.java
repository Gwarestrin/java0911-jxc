package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author : Ziming
 * date : 2023/9/11
 * description: 客户信息
 */
public interface CustomerDao {
    /**
     * 分页
     *
     * @param offSet
     * @param rows
     * @param customerName
     * @return
     */
    List<Customer> getCustomerList(@Param("offSet") int offSet, @Param("rows") Integer rows, @Param("customerName") String customerName);

    Integer getCustomerCount(@Param("customerName") String customerName);

    Customer findCustomerByName(@Param("customerName") String customerName);

    Integer addCustomer(@Param("customer") Customer customer);

    Integer updateCustomer(Customer customer, Integer customerId);

    Integer batchDeleteByIds(List<Integer> list);
}
