package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Unit;

import java.util.List;

/**
 * @author : Ziming
 * date : 2023/9/11
 * description:
 */
public interface UnitDao {
    List<Unit> getUnitList();
}
