package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author : Ziming
 * date : 2023/9/12
 * description:
 */
public interface DamageListDao {
    Integer save(@Param("damageList") DamageList damageList);

    List<DamageList> list(@Param("sTime") String sTime, @Param("eTime") String eTime);
}
