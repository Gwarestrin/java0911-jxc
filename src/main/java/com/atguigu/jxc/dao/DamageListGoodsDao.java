package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author : Ziming
 * date : 2023/9/12
 * description:
 */
public interface DamageListGoodsDao {

    Integer saveBatch(@Param("damageListGoodsList") List<DamageListGoods> damageListGoodsList);

    List<DamageListGoods> goodsList(@Param("damageListId") Integer damageListId);
}
