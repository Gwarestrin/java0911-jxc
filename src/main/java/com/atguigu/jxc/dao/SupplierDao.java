package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author : Ziming
 * date : 2023/9/11
 * description: 供应商
 */
public interface SupplierDao {
    List<Supplier> getSupplierList(@Param("offSet")int offSet, @Param("rows")Integer rows, @Param("supplierName")String supplierName);

    /**
     * 虽然只有一个参数，但xml里面用到了if标签，所以需要加上 @Param("supplierName")
     */
    Integer getSupplierCount(@Param("supplierName") String supplierName);

    Supplier findSupplierByName(@Param("supplierName") String supplierName);

    /**
     * 新增供应商
     * @param supplier
     * @return
     */
    Integer addSupplier(@Param("supplier") Supplier supplier);

    /**
     * 修改供应商
     * 因为传入了两个参数，xml里面需要用supplier.属性名
     * 这里不需要@param可能是因为我们在xml里面规定了parameterType
     * @param supplier
     * @return
     */
    Integer updateSupplier(@Param("supplier") Supplier supplier, @Param("supplierId") Integer supplierId);

    Integer batchDeleteByIds(List<Integer> list);
}
