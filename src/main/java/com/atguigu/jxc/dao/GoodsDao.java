package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {

    String getMaxCode();

    List<Goods> getGoodsList(@Param("offSet") int offSet, @Param("rows") Integer rows, @Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);

    Integer getGoodsCount(@Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);

    Integer addGoods(@Param("goods") Goods goods);

    Integer updateGoods(@Param("goods") Goods goods, @Param("goodsId") Integer goodsId);


    Goods getGoodsById(@Param("goodsId") Integer goodsId);

    Integer delete(@Param("goodsId") Integer goodsId);

    List<Goods> getInventoryList(@Param("offSet") int offSet, @Param("rows") Integer rows,
                                 @Param("nameOrCode") String nameOrCode, @Param("hasInventory") Boolean hasInventory);

    Integer getNoInventoryCount(@Param("nameOrCode") String nameOrCode, @Param("hasInventory") Boolean hasInventory);

    Integer deleteStock(@Param("goodsId") Integer goodsId);

    List<Goods> listAlarm();
}
