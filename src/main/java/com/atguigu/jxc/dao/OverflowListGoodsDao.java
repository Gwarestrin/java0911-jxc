package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author : Ziming
 * date : 2023/9/12
 * description:
 */
public interface OverflowListGoodsDao {
    Integer saveBatch(@Param("overflowListGoodsList") List<OverflowListGoods> overflowListGoodsList);

    List<OverflowListGoods> goodsList(@Param("overflowListId") Integer overflowListId);
}
