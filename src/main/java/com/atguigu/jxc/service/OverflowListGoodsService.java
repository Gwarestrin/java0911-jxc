package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author : Ziming
 * date : 2023/9/12
 * description:
 */
public interface OverflowListGoodsService {
    ServiceVO save(OverflowList overflowList, String overflowListGoodsStr, String overflowNumber, HttpSession session);

    Map<String, Object> list(String sTime, String eTime);

    Map<String, Object> goodsList(Integer overflowListId);
}
