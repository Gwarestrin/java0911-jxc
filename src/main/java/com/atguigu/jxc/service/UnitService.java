package com.atguigu.jxc.service;

import java.util.Map;

/**
 * @author : Ziming
 * date : 2023/9/11
 * description:
 */
public interface UnitService {

    Map<String, Object> list();
}
