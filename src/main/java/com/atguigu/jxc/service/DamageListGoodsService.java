package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author : Ziming
 * date : 2023/9/12
 * description:
 */
public interface DamageListGoodsService {
    ServiceVO save(DamageList damageList, String damageListGoodsStr, String damageNumber, HttpSession session);

    Map<String, Object> list(String sTime, String eTime);

    Map<String, Object> goodsList(Integer damageListId);
}
