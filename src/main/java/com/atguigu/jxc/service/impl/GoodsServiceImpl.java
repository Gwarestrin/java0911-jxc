package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerReturnListGoodsDao;
import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.dao.SaleListGoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.CustomerReturnListGoods;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.SaleListGoods;
import com.atguigu.jxc.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Autowired
    private GoodsTypeDao goodsTypeDao;

    @Autowired
    private SaleListGoodsDao saleListGoodsDao;

    @Autowired
    private CustomerReturnListGoodsDao customerReturnListGoodsDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for (int i = 4; i > intCode.toString().length(); i--) {

            unitCode = "0" + unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {

        try {
            if (goodsTypeId == 1) {
                goodsTypeId = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Goods> goods = goodsDao.getGoodsList(offSet, rows, codeOrName, goodsTypeId);

        // 获取销售列表
        List<SaleListGoods> saleListGoods = saleListGoodsDao.getSaleListGoods(null, null, null);
        // 遍历销售列表集合，并以GoodsId为key、以GoodsNum的和为value，转换为map集合
        Map<Integer, Integer> saleSum = saleListGoods.stream().collect(Collectors.groupingBy(
                SaleListGoods::getGoodsId,
                Collectors.summingInt(SaleListGoods::getGoodsNum)
        ));

        // 获取退货列表
        List<CustomerReturnListGoods> customerReturnListGoods
                = customerReturnListGoodsDao.getCustomerReturnListGoods(null, null, null);
        // 遍历退货列表集合，并以GoodsId为key、以GoodsNum的和为value，转换为map集合
        Map<Integer, Integer> returnSum = customerReturnListGoods.stream().collect(Collectors.groupingBy(
                CustomerReturnListGoods::getGoodsId,
                Collectors.summingInt(CustomerReturnListGoods::getGoodsNum)
        ));

        String goodsTypeName;
        Integer goodsId;
        Integer saleNum;
        Integer returnNum;

        for (Goods good : goods) {
            goodsTypeName = goodsTypeDao.getTypeNameByTypeId(good.getGoodsTypeId());
            good.setGoodsTypeName(goodsTypeName);

            goodsId = good.getGoodsId();

            // 计算SaleTotal净销售量
            if (saleSum.containsKey(goodsId) || returnSum.containsKey(goodsId)) {
                // 获取销售数量
                saleNum = saleSum.get(goodsId);
                // 获取退货数量
                returnNum = returnSum.get(goodsId);
                if ("".equals(saleNum) || saleNum == null) {
                    saleNum = 0;
                }
                if ("".equals(returnNum) || returnNum == null) {
                    returnNum = 0;
                }
                good.setSaleTotal(saleNum - returnNum);
            }
        }

        map.put("total", goodsDao.getGoodsCount(codeOrName, goodsTypeId));
        map.put("rows", goods);

        return map;
    }

    @Override
    public ServiceVO save(Goods goods, Integer goodsId) {
        if (goodsId == null) {
            // 新增
            goodsDao.addGoods(goods);
        } else {
            // 修改
            goodsDao.updateGoods(goods, goodsId);
        }

        String typeName = goodsTypeDao.getTypeNameByTypeId(goods.getGoodsTypeId());
        goods.setGoodsTypeName(typeName);

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO delete(Integer goodsId) {

        Goods goods = goodsDao.getGoodsById(goodsId);

        if (goods.getState() == 0) {
            goodsDao.delete(goodsId);
        } else {
            // State=1或2 => 入库、有进货和销售单据的商品不能删除
            return new ServiceVO(ErrorCode.GOODS_DEL_ERROR_CODE, ErrorCode.GOODS_DEL_ERROR_MESS);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> getInventoryQuantity(Integer page, Integer rows, String nameOrCode, Boolean hasInventory) {
        Map<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Goods> goods = goodsDao.getInventoryList(offSet, rows, nameOrCode, hasInventory);

        for (Goods good : goods) {
            String goodsTypeName = goodsTypeDao.getTypeNameByTypeId(good.getGoodsTypeId());
            good.setGoodsTypeName(goodsTypeName);
        }

        map.put("total", goodsDao.getNoInventoryCount(nameOrCode, hasInventory));
        map.put("rows", goods);

        return map;

    }

    /**
     * 添加库存、修改数量或成本价
     *
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     * @return
     */
    @Override
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {

        Goods goods = goodsDao.getGoodsById(goodsId);
        goods.setInventoryQuantity(inventoryQuantity);
        goods.setPurchasingPrice(purchasingPrice);

        goodsDao.updateGoods(goods, goodsId);

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO deleteStock(Integer goodsId) {

        Goods goods = goodsDao.getGoodsById(goodsId);

        goodsDao.deleteStock(goodsId);

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 查询所有 当前库存量 小于 库存下限的商品信息
     *
     * @return
     */
    @Override
    public Map<String, Object> listAlarm() {
        Map<String, Object> map = new HashMap<>();

        List<Goods> goods = goodsDao.listAlarm();

        for (Goods good : goods) {
            String goodsTypeName = goodsTypeDao.getTypeNameByTypeId(good.getGoodsTypeId());
            good.setGoodsTypeName(goodsTypeName);
        }

        map.put("rows", goods);
        return map;
    }

}
