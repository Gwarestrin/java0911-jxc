package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author : Ziming
 * date : 2023/9/11
 * description: 客户
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String customerName) {
        Map<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Customer> customers = customerDao.getCustomerList(offSet, rows, customerName);

        map.put("total", customerDao.getCustomerCount(customerName));
        map.put("rows", customers);

        return map;
    }

    @Override
    public ServiceVO save(Customer customer, Integer customerId) {
        if (customerId == null) {
            // 新增
            // 判断客户名字是否重复
            Customer exCustomer = customerDao.findCustomerByName(customer.getCustomerName());

            if (exCustomer != null) {
                return new ServiceVO(ErrorCode.CUSTOMER_NAME_EXIST_CODE, ErrorCode.CUSTOMER_NAME_EXIST_MESS);
            }
            customerDao.addCustomer(customer);

        } else {
            // 修改
            customerDao.updateCustomer(customer, customerId);
        }

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

    }

    @Override
    public ServiceVO delete(String ids) {
        List<Integer> list = Arrays.stream(ids.split(","))
                .map(Integer::parseInt)
                .collect(Collectors.toList());

        customerDao.batchDeleteByIds(list);

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

    }
}
