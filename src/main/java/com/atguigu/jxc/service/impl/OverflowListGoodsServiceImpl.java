package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.OverflowListDao;
import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.entity.vo.ListGoodsVO;
import com.atguigu.jxc.service.OverflowListGoodsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author : Ziming
 * date : 2023/9/12
 * description:
 */
@Service
public class OverflowListGoodsServiceImpl implements OverflowListGoodsService {

    @Autowired
    private OverflowListDao overflowListDao;

    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao;

    @Autowired
    private UserDao userDao;

    @Override
    public ServiceVO save(OverflowList overflowList, String overflowListGoodsStr, String overflowNumber, HttpSession session) {
        User user = (User) session.getAttribute("currentUser");
        overflowList.setUserId(user.getUserId());
        overflowList.setOverflowNumber(overflowNumber);

        overflowListDao.save(overflowList);

        Gson gson = new Gson();
        List<ListGoodsVO> listGoodsVOList = gson.fromJson(overflowListGoodsStr, new TypeToken<List<ListGoodsVO>>() {
        }.getType());

        List<OverflowListGoods> overflowListGoodsList = listGoodsVOList.stream()
                .map(listGoodsVO -> {
                    OverflowListGoods overflowListGoods = new OverflowListGoods();
                    BeanUtils.copyProperties(listGoodsVO, overflowListGoods);
                    overflowListGoods.setOverflowListId(overflowList.getOverflowListId());
                    return overflowListGoods;
                }).collect(Collectors.toList());

        overflowListGoodsDao.saveBatch(overflowListGoodsList);

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        Map<String, Object> map = new HashMap<>();

        // 查出damageDate介于sTime和eTime的报损单列表
        List<OverflowList> overflowList = overflowListDao.list(sTime,eTime);

        // 根据userId设置trueName
        User user;
        for (OverflowList list : overflowList) {
            user = userDao.getUserById(list.getUserId());
            list.setTrueName(user.getTrueName());
        }

        map.put("rows", overflowList);
        return map;

    }

    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {
        Map<String, Object> map = new HashMap<>();

        List<OverflowListGoods> overflowListGoodsList = overflowListGoodsDao.goodsList(overflowListId);

        map.put("rows", overflowListGoodsList);
        return map;
    }
}
