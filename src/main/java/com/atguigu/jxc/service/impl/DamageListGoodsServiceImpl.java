package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.entity.vo.ListGoodsVO;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author : Ziming
 * date : 2023/9/12
 * description:
 */
@Service
public class DamageListGoodsServiceImpl implements DamageListGoodsService {

    @Autowired
    private DamageListDao damageListDao;

    @Autowired
    private DamageListGoodsDao damageListGoodsDao;

    @Autowired
    private UserDao userDao;

    @Override
    @Transactional
    public ServiceVO save(DamageList damageList, String damageListGoodsStr, String damageNumber, HttpSession session) {
        // 存入 damageList
        // 据说这里只需要用到damageList一个参数，damageNumber可以自动装配进去
        User user = (User) session.getAttribute("currentUser");
        damageList.setUserId(user.getUserId());
        damageList.setDamageNumber(damageNumber);
        // 新增 damageList
        damageListDao.save(damageList);


        // 把 damageListGoodsStr 转换成 DamageListGoodsVO对象 (项目已导入gson)
        Gson gson = new Gson();
        List<ListGoodsVO> listGoodsVOList = gson.fromJson(damageListGoodsStr, new TypeToken<List<ListGoodsVO>>() {
        }.getType());

        // 实际上前端能改的字段只有price、goodsNum、remarks
        // 将属性复制到damageListGoods里（price、goodsNum）
        /*DamageListGoods damageListGoods;

        for (DamageListGoodsVO damageListGoodsVO : damageListGoodsVOList) {
            damageListGoods = new DamageListGoods();

            BeanUtils.copyProperties(damageListGoodsVO, damageListGoods);

            // 将刚才插入damageList后自动生成的damageListId放进damageListGoods
            damageListGoods.setDamageListId(damageList.getDamageListId());

            // 根据damageListGoodsId保存damageListGoods
            damageListGoodsDao.save(damageListGoods);
        }*/

        List<DamageListGoods> damageListGoodsList = listGoodsVOList.stream()
                .map(listGoodsVO -> {
                    DamageListGoods damageListGoods = new DamageListGoods();
                    // 拷贝属性
                    BeanUtils.copyProperties(listGoodsVO, damageListGoods);
                    // 将damageListId放进damageListGoods
                    damageListGoods.setDamageListId(damageList.getDamageListId());
                    return damageListGoods;
                }).collect(Collectors.toList());

        damageListGoodsDao.saveBatch(damageListGoodsList);

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        Map<String, Object> map = new HashMap<>();

        // 查出damageDate介于sTime和eTime的报损单列表
        List<DamageList> damageList = damageListDao.list(sTime,eTime);

        // 根据userId设置trueName
        User user;
        for (DamageList list : damageList) {
            user = userDao.getUserById(list.getUserId());
            list.setTrueName(user.getTrueName());
        }

        map.put("rows", damageList);
        return map;

    }

    @Override
    public Map<String, Object> goodsList(Integer damageListId) {
        Map<String, Object> map = new HashMap<>();

        List<DamageListGoods> damageListGoodsList = damageListGoodsDao.goodsList(damageListId);

        map.put("rows", damageListGoodsList);
        return map;
    }
}
