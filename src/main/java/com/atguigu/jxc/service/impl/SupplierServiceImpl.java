package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author : Ziming
 * date : 2023/9/11
 * description:
 */
@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierDao supplierDao;


    @Override
    public Map<String, Object> list(Integer page, Integer rows, String supplierName) {
        Map<String, Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Supplier> suppliers = supplierDao.getSupplierList(offSet, rows, supplierName);

        map.put("total", supplierDao.getSupplierCount(supplierName));
        map.put("rows", suppliers);

        return map;

    }

    @Override
    public ServiceVO save(Supplier supplier, Integer supplierId) {

        if (supplierId == null) {
            // 新增
            // 判断供应商名字是否重复
            Supplier exSupplier = supplierDao.findSupplierByName(supplier.getSupplierName());

            if (exSupplier != null) {
                return new ServiceVO(ErrorCode.SUPPLIER_NAME_EXIST_CODE, ErrorCode.SUPPLIER_NAME_EXIST_MESS);
            }
            supplierDao.addSupplier(supplier);

        } else {
            // 修改
            supplierDao.updateSupplier(supplier, supplierId);
        }

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

    }

    @Override
    public ServiceVO delete(String ids) {

        List<Integer> list = Arrays.stream(ids.split(","))
                .map(Integer::parseInt)
                .collect(Collectors.toList());

        supplierDao.batchDeleteByIds(list);

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }
}
