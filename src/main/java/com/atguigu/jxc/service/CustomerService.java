package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;

import java.util.Map;

/**
 * @author : Ziming
 * date : 2023/9/11
 * description: 客户
 */
public interface CustomerService {
    /**
     * 分页
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    Map<String, Object> list(Integer page, Integer rows, String customerName);

    ServiceVO save(Customer customer, Integer customerId);

    ServiceVO delete(String ids);
}
