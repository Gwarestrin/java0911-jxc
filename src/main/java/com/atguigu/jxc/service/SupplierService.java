package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

/**
 * @author : Ziming
 * date : 2023/9/11
 * description:
 */
public interface SupplierService {
    /**
     * 分页查询供应商列表
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    Map<String, Object> list(Integer page, Integer rows, String supplierName);

    ServiceVO save(Supplier supplier, Integer supplierId);

    ServiceVO delete(String ids);
}
