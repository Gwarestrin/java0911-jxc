package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author : Ziming
 * date : 2023/9/11
 * description: 供应商Controller控制器
 */
@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    private SupplierService supplierService;

    /**
     * 分页查询供应商
     *
     * @param page         当前页数
     * @param rows         每页显示的记录数
     * @param supplierName 供应商名
     * @return
     */
    @RequestMapping("/list")
    public Map<String, Object> list(Integer page, Integer rows, String supplierName) {
        return supplierService.list(page, rows, supplierName);
    }

    /**
     * 新增或修改
     *
     * @param supplier
     * @param supplierId
     * @return
     */
    @RequestMapping("/save")
    public ServiceVO save(Supplier supplier, @RequestParam(value = "supplierId", required = false) Integer supplierId) {
        return supplierService.save(supplier, supplierId);
    }

    /**
     * 批量删除
     */
    @RequestMapping("/delete")
    public ServiceVO delete(String ids) {
        return supplierService.delete(ids);
    }
}
