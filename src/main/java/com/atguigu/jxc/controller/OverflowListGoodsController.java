package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.atguigu.jxc.service.OverflowListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author : Ziming
 * date : 2023/9/12
 * description:
 */
@RestController
@RequestMapping("/overflowListGoods")
public class OverflowListGoodsController {

    @Autowired
    private OverflowListGoodsService overflowListGoodsService;

    @RequestMapping("/save")
    public ServiceVO save(OverflowList overflowList, String overflowListGoodsStr, HttpSession session,
                          @RequestParam(value = "overflowNumber", required = false) String overflowNumber) {

        return overflowListGoodsService.save(overflowList, overflowListGoodsStr, overflowNumber, session);
    }

    @RequestMapping("/list")
    public Map<String, Object> list(String sTime, String eTime) {
        return overflowListGoodsService.list(sTime, eTime);
    }


    @RequestMapping("/goodsList")
    public Map<String, Object> goodsList(Integer overflowListId) {
        return overflowListGoodsService.goodsList(overflowListId);
    }

}
