package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author : Ziming
 * date : 2023/9/11
 * description: 客户Controller
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    /**
     * 客户列表分页（名称模糊查询）
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    @RequestMapping("/list")
    public Map<String, Object> GoodsPageList(Integer page, Integer rows, String  customerName) {

        return customerService.list(page, rows, customerName);
    }

    /**
     * 新增或修改
     * @param customer
     * @param customerId
     * @return
     */
    @RequestMapping("/save")
    public ServiceVO save(Customer customer, @RequestParam(value = "customerId", required = false) Integer customerId) {

        return customerService.save(customer, customerId);
    }

    /**
     * 批量删除
     */
    @RequestMapping("/delete")
    public ServiceVO delete(String ids) {
        return customerService.delete(ids);
    }

}
