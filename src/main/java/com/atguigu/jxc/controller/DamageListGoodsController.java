package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author : Ziming
 * date : 2023/9/12
 * description:
 */
@RestController
@RequestMapping("/damageListGoods")
public class DamageListGoodsController {

    @Autowired
    private DamageListGoodsService damageListGoodsService;

    @RequestMapping("/save")
    public ServiceVO save(DamageList damageList, String damageListGoodsStr, HttpSession session,
                          @RequestParam(value = "damageNumber", required = false) String damageNumber) {

        return damageListGoodsService.save(damageList, damageListGoodsStr, damageNumber, session);
    }

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @RequestMapping("/list")
    public Map<String, Object> list(String sTime, String eTime) {
        return damageListGoodsService.list(sTime, eTime);
    }

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    @RequestMapping("/goodsList")
    public Map<String, Object> goodsList(Integer damageListId) {
        return damageListGoodsService.goodsList(damageListId);
    }

}
