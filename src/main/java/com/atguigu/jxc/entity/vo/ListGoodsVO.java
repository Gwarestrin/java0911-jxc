package com.atguigu.jxc.entity.vo;

import lombok.Data;

/**
 * @author : Ziming
 * date : 2023/9/12
 * description:
 */
@Data
public class ListGoodsVO {

    private Integer goodsId;
    private Integer goodsTypeId;
    private String goodsCode;
    private String goodsName;
    private String goodsModel;
    private String goodsUnit;
    private double lastPurchasingPrice;
    private double price;
    private Integer goodsNum;
    private double total;
}
